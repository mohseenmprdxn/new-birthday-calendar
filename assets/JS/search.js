import catching from "./catchDay.js"
import styling from "./dynamicStyling.js";

export default class search {
  constructor() {
    this.jsonData = ".json-data";
    this.btn = ".submit";
    this.yearClass = ".year";
    this.year = 0;
    this.jsonArray = {};
    this.collectDays = [];
  }


  complete = () => {
    this.makeEvent()
  }
// click event on update button
  makeEvent = () => {
    let tap = document.querySelector(this.btn);
    tap.addEventListener("click", this.makeClick);
  }

  // Taking json data from the textarea
  takeJsonData = () => {
    let collect = document.querySelector(this.jsonData);
    this.jsonArray = JSON.parse(collect.value);
    return true;
  }

  // Taking year from input section
  takeYear = () => {
    let numb = document.querySelector(this.yearClass);
    this.gotYear = numb.value;
    let regex = /^[0-9]{4}$/gi;

    if (!regex.test(this.gotYear)) {
      alert("Invalid Year");
      return false;
    } else {
      this.gotYear = Number(this.gotYear)
      return true;
    }
  }

  // taking name and date from json.
  workingJson = () => {
    let json = this.jsonArray;
    json.forEach(elem => {
      let { name, birthday } = elem;
      let initials = this.takeInitial(name);
      let thatDay = catching(birthday, this.year);
      
      if (thatDay !== false) {
        this.collectDays.push({ thatDay, initials });
      }
    });
  }

  // taking the initials of the name
  takeInitial = (name) => {
    let char = name.split(" ");
    if (char.length < 2)
      return name[0].toUpperCase() + name[1].toUpperCase();
    return char[0][0].toUpperCase() + char[1][0].toUpperCase();
  }
// reset for the update click
  reset = () => {
    this.collectDays = [];
  }

  // making the click event
  makeClick = () => {
    this.reset();
    let json = this.takeJsonData();
    let yearNumber = this.takeYear();

    if (json && yearNumber) {
      this.workingJson();
      let asigning = new styling(this.collectDays);
      asigning.asign();
    }
  }
}