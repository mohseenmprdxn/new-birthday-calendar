export default class styling {
    constructor(data) {
      this.data = data;
      this.elems = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
      this.colors = ["red", "lightblue", "green", "yellow", "orange"];
      this.count = new Array(7).fill(0);
    }
  
    // Assigning elements on the page
    asign = () => {
      this.clear();
  
      this.data.forEach(elem => {
        let { thatDay } = elem;
        this.count[thatDay] += 1;
      });
  
      this.data.forEach(elem => {
        let { thatDay, initials } = elem;
        this.addbox(thatDay, initials);
      });
    }
  
    // Adding names to the calendar
    addbox = (target, initials) => {
      let targetDay = this.elems[target];
      let targetElem = document.querySelector(`.week-${targetDay}`);
      let span = document.createElement("span");
      span.innerText = initials;
  
      // Adding styles;
      let color = this.colors[Math.floor(Math.random() * this.colors.length)];
      let css = `background-color:${color};`;
  
      if (this.count[target] != 1) {        // assigning styles to the number  
        targetElem.setAttribute("style", "align-content:start");
      }
  
      targetElem.removeAttribute("style");  // Removing already having style
  
      if (this.count[target] < 4) {
        css += `flex-basis: ${100 / this.count[target]}%;`;
      } else {
        css += "flex-basis: 25%;";
      }
      if (this.count[target] <= 3) {
        css += `flex-basis: 50%;`;
      }
      if (this.count[target] == 1) {
        css += `flex-basis: 100%;`
      }
  
      span.setAttribute("style", css);
      targetElem.appendChild(span);
    }
  
    // Clear elements from calendar
    clear = () => {
      this.elems.forEach(elem => {
        let tag = document.querySelector(`.week-${elem}`);
  
        while (tag.childNodes.length > 0) {
          tag.removeChild(tag.lastElementChild);
        }
      })
    }
  }